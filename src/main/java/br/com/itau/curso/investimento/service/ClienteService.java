package br.com.itau.curso.investimento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.curso.investimento.model.Cliente;
import br.com.itau.curso.investimento.repository.ClienteRepository;

@Service

public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public void criarCliente(Cliente cliente) {
		clienteRepository.save(cliente);
	}

	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienteRepository.delete(cliente);
	}
	
	public Iterable<Cliente> obterClientes() {
		return clienteRepository.findAll();
	}
	
	public Cliente encontraOuDaErro(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}

		return optional.get();
	}
	
}
